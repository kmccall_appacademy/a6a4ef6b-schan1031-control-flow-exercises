# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  al = 'abcdefghijklmnopqrstuvwxyz'
  al.chars.each do |l|
    str.delete!(l)
  end
  return str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 == 0
    return str[str.length/2-1..str.length/2]
  else
    return str[str.length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  ct = 0
  str.chars.each do |el|
    if VOWELS.include?(el)
      ct += 1
    end
  end
  return ct
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  n = [*1..num].reduce(&:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  s = ""
  arr.each do |el|
    s = s + el + separator
  end
  if separator == ""
    return s
  else
    return s[0..-2]
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  copy = str
  str.chars.each_index do |idx|
    if idx % 2 == 0
      copy[idx] = copy[idx].downcase
    else
      copy[idx] = copy[idx].upcase
    end
  end
  return copy
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.each do |word|
    if word.length >= 5
      word = word.reverse!
    end
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  v = [*1..n]
  v.each do |val|
    if val % 3 == 0
      if val % 5 == 0
        v[val-1] = 'fizzbuzz'
      else
        v[val-1] = 'fizz'
      end
    elsif val % 5 == 0
      v[val-1] = 'buzz'
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  f = [*2..num]
  f.each do |val|
    if num % val != 0
      f.delete(val)
    end
  end
  if f.length > 0 || f != [2] || num == 1
    false
  else
    true
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)

end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)

end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)

end
